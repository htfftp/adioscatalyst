#include "FEDataStructures.h"

#include <cmath>
#include <iostream>
#include <iterator>

#include <mpi.h>

#include <fstream>

Grid::Grid() = default;

void Grid::Initialize(const unsigned int numPoints[3], const double spacing[3])
{
  if (numPoints[0] == 0 || numPoints[1] == 0 || numPoints[2] == 0)
  {
    std::cerr << "Must have a non-zero amount of points in each direction.\n";
  }

  this->Dims[0] = numPoints[0];
  this->Dims[1] = numPoints[1];
  this->Dims[2] = numPoints[2];

  int numberOfPts = numPoints[0] * numPoints[1] * numPoints[2];
  int mpiNumOfPts[3] = {};

  int mpiSize = 1;
  int mpiRank = 0;

  int wrank;
  MPI_Comm comm, cart_comm;

  // in parallel, we do a simple partitioning in the x-direction.
  MPI_Comm_rank(MPI_COMM_WORLD, &wrank);

  const unsigned int color = 1;
  MPI_Comm_split(MPI_COMM_WORLD, color, wrank, &comm);

  MPI_Comm_rank(comm, &mpiRank);
  MPI_Comm_size(comm, &mpiSize);

  int periods[1] = { 1 };
  int coords[1] = { 1 };

  // Dimension of local array
  std::vector<size_t> size;
  // Offset of local array in the global array
  std::vector<size_t> offsetMPI;

  MPI_Dims_create(mpiSize, 1, mpiNumOfPts);

  MPI_Cart_create(comm, 1, mpiNumOfPts, periods, 0, &cart_comm);
  MPI_Cart_coords(cart_comm, mpiRank, 1, coords);

  size.emplace_back(numberOfPts / mpiNumOfPts[0]);
  if (coords[0] < numberOfPts % mpiNumOfPts[0])
  {
    size[0]++;
  }
  offsetMPI.emplace_back(
    (numberOfPts / mpiNumOfPts[0] * coords[0]) + std::min(numberOfPts % mpiNumOfPts[0], coords[0]));
  
  int maxPointOnRank = size[0];
  int pointsOffset = offsetMPI[0];

  unsigned int startXPoint = mpiRank * numPoints[0] / mpiSize;
  unsigned int endXPoint = (mpiRank + 1) * numPoints[0] / mpiSize;
  if (mpiSize != mpiRank + 1)
  {
    endXPoint++;
  }

  int count = 0;
  double coord[3] = { 0, 0, 0 };
  for (unsigned int i = 0; i < endXPoint; i++)
  {
    coord[0] = i * spacing[0];
    for (unsigned int j = 0; j < numPoints[1]; j++)
    {
      coord[1] = j * spacing[1];
      for (unsigned int k = 0; k < numPoints[2]; k++)
      {
        count++;
        if (count <= pointsOffset || count > (pointsOffset + maxPointOnRank))
        {
          continue;
        }

        coord[2] = k * spacing[2];
        // add the coordinate to the end of the vector
        std::copy(coord, coord + 3, std::back_inserter(this->Points));
      }
    }
  }

  // create the hex cells
  unsigned int cellPoints[8];
  unsigned int numXPoints = endXPoint - startXPoint;
  unsigned int cellOffset = startXPoint * numPoints[1] * numPoints[2];

  this->CellsShape = (numPoints[0]) * (numPoints[1] - 1) * (numPoints[2] - 1);
  this->CellsStart = startXPoint * (numPoints[1] - 1) * (numPoints[2] - 1);
  this->CellsCount = (numXPoints) * (numPoints[1] - 1) * (numPoints[2] - 1);

  for (unsigned int i = 0; i < numXPoints - 1; i++)
  {
    for (unsigned int j = 0; j < numPoints[1] - 1; j++)
    {
      for (unsigned int k = 0; k < numPoints[2] - 1; k++)
      {
        cellPoints[0] = i * numPoints[1] * numPoints[2] + j * numPoints[2] + k + cellOffset;
        cellPoints[1] = (i + 1) * numPoints[1] * numPoints[2] + j * numPoints[2] + k + cellOffset;
        cellPoints[2] =
          (i + 1) * numPoints[1] * numPoints[2] + (j + 1) * numPoints[2] + k + cellOffset;
        cellPoints[3] = i * numPoints[1] * numPoints[2] + (j + 1) * numPoints[2] + k + cellOffset;
        cellPoints[4] = i * numPoints[1] * numPoints[2] + j * numPoints[2] + k + 1 + cellOffset;
        cellPoints[5] =
          (i + 1) * numPoints[1] * numPoints[2] + j * numPoints[2] + k + 1 + cellOffset;
        cellPoints[6] =
          (i + 1) * numPoints[1] * numPoints[2] + (j + 1) * numPoints[2] + k + 1 + cellOffset;
        cellPoints[7] =
          i * numPoints[1] * numPoints[2] + (j + 1) * numPoints[2] + k + 1 + cellOffset;
        std::copy(cellPoints, cellPoints + 8, std::back_inserter(this->Cells));
      }
    }
  }
}

size_t Grid::GetNumberOfPoints()
{
  return this->Points.size() / 3;
}

size_t Grid::GetNumberOfCells()
{
  return this->Cells.size() / 8;
}

double* Grid::GetPointsArray()
{
  if (this->Points.empty())
  {
    return nullptr;
  }
  return &(this->Points[0]);
}

double* Grid::GetPoint(size_t pointId)
{
  if (pointId >= this->Points.size())
  {
    return nullptr;
  }
  return &(this->Points[pointId * 3]);
}

unsigned int* Grid::GetCellPoints(size_t cellId)
{
  if (cellId >= this->Cells.size())
  {
    return nullptr;
  }
  return &(this->Cells[cellId * 8]);
}

int Grid::GetCellsShape()
{
  return this->CellsShape;
}

int Grid::GetCellsStart()
{
  return this->CellsStart;
}

int Grid::GetCellsCount()
{
  return this->CellsCount;
}

size_t* Grid::GetDims()
{
  return this->Dims;
}

Attributes::Attributes()
{
  this->GridPtr = nullptr;
}

void Attributes::Initialize(Grid* grid)
{
  this->GridPtr = grid;
}

void Attributes::UpdateFields(double time)
{
  size_t numPoints = this->GridPtr->GetNumberOfPoints();
  this->Velocity.resize(numPoints * 3);
  for (size_t pt = 0; pt < numPoints; pt++)
  {
    double* coord = this->GridPtr->GetPoint(pt);
    this->Velocity[pt] = coord[0] * time;
    this->Velocity[pt + numPoints] = coord[0] + coord[1] * time;
    this->Velocity[pt + numPoints * 2] = coord[0] + coord[2] * (time);
  }
  size_t numCells = this->GridPtr->GetNumberOfCells();

  this->Pressure.resize(numCells);
  std::fill(this->Pressure.begin(), this->Pressure.end(), 1);
}

double* Attributes::GetVelocityArray()
{
  if (this->Velocity.empty())
  {
    return nullptr;
  }
  return &this->Velocity[0];
}

float* Attributes::GetPressureArray()
{
  if (this->Pressure.empty())
  {
    return nullptr;
  }
  return &this->Pressure[0];
}
