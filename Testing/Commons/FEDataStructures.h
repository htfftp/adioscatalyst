#ifndef FEDATASTRUCTURES_HEADER
#define FEDATASTRUCTURES_HEADER

#include <cstddef>
#include <vector>

class Grid
{
public:
  Grid();
  void Initialize(const unsigned int numPoints[3], const double spacing[3]);
  size_t GetNumberOfPoints();
  size_t GetNumberOfCells();
  double* GetPointsArray();
  double* GetPoint(size_t pointId);
  size_t* GetDims();
  unsigned int* GetCellPoints(size_t cellId);
  int GetCellsShape();
  int GetCellsStart();
  int GetCellsCount();

private:
  std::vector<double> Points;
  std::vector<unsigned int> Cells;

  unsigned int CellsShape = 0;
  unsigned int CellsStart = 0;
  unsigned int CellsCount = 0;

  size_t Dims[3] = { 0, 0, 0 };
};

class Attributes
{
  // A class for generating and storing point and cell fields.
  // Velocity is stored at the points and pressure is stored
  // for the cells. The current velocity profile is for a
  // flow with U(y,t) = y*t, V = 0 and W = y + z * t.
  // Pressure is constant through the domain.
public:
  Attributes();
  void Initialize(Grid* grid);
  void UpdateFields(double time);
  double* GetVelocityArray();
  float* GetPressureArray();

private:
  std::vector<double> Velocity;
  std::vector<float> Pressure;
  Grid* GridPtr;
};
#endif
