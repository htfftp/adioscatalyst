# AdiosCatalyst

AdiosCatalyst is a new [Catalyst](https://gitlab.kitware.com/paraview/catalyst) implementation for [Adios2](https://github.com/ornladios/ADIOS2). The aim of this specific implementation is to be able to make in-transit simulation with the [SST](https://adios2.readthedocs.io/en/latest/engines/engines.html#sst-sustainable-staging-transport) engine of Adios2.


For more details, the initial design of this implementation of Catalyst2 with Adios2 has been described [here](https://discourse.paraview.org/t/in-transit-catalyst-implementation-based-on-adios2/9718).

## Requirements

need to build and install:
- [ADIOS2](https://adios2.readthedocs.io/en/latest/) : version 2.8 or newer.
- [Catalyst2](https://catalyst-in-situ.readthedocs.io/en/latest/) : from this [commit](https://gitlab.kitware.com/paraview/catalyst/-/commit/431a8a1b1bf43ca72ec041e2ccf57622982880bc) or newer.

Note that you also need `MPI` because it's required by Adios2.

## Getting started

This implementation works in two steps, first for the simulation node, there is (in `AdiosCatalyst`) the specific catalyst implementation which converts conduit node created in your simulation to Adios2. Second, for the analysis, there is an executable (in `AdiosReplay`) which do the opposite operation and delegate the visualization to another catalyst implementation named `ParaviewCatalyst`.

### Simulation Nodes


- first, you will need to specify several environment variables for Catalyst2:

```bash
export CATALYST_IMPLEMENTATION_PATHS="<AdiosCatalyst-build-location>/lib/catalyst" \
export CATALYST_IMPLEMENTATION_NAME=adios
```

- then, start the simulation, an example can be find in the `Testing` folder named `TestWriter` :

```
build/<your-simulation-name> <adios-xml-config-file> Testing/Commons/catalyst_pipeline.py
```

### Analysis Nodes

This part implements an adios2 reader to read Adios2 data and recreates conduit node for Catalyst2, as output we trigger another catalyst implementation for the visualization.
Currently we use the ParaView implementation of Catalyst2 for that.

- Specify the environment variables `CATALYST_IMPLEMENTATION_PATHS` for Catalyst2:

```bash
export CATALYST_IMPLEMENTATION_PATHS="<ParaViewCatalyst-build-location>/lib/catalyst"
```

- To start the visualization:

```
bin/AdiosReplay ../adioscatalyst/Testing/adios2.xml
```

Note that you'll need also to set the `RendezvousReaderCount` to the number of node you want.

## Limitations

- The main difference between catalyst2 and adios2 is the `Initialize` method. For adios2, to define a variable we need to know its dimensions but we cannot access them in Catalyst `Initialize`. So we define these variables during the first `Execute` called.

- As Adios engine support only [scalar variables](https://adios2.readthedocs.io/en/latest/components/components.html#data-types), for vector fields we need to split it by components.

- Some additional parameters should be passed through the catalyst node in the `CatalystAdaptor` :
  - point number in `coordsets/coords/number_of_points`.
  - cell number in  `topologies/mesh/elements/number_of_cell`.
  - with mpi, we have to define other parameters :
    - cell shape in  `topologies/mesh/elements/cells_shape`.
    - cell start in  `topologies/mesh/elements/cells_start`.
    - cell count in  `topologies/mesh/elements/cells_count`.

## License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.
