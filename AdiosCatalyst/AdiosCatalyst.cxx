#include "AdiosPipeline.h"

#include "catalyst_impl_adios.h"

#include <catalyst.h>
#include <catalyst_api.h>
#include <catalyst_conduit.hpp>
#include <catalyst_conduit_blueprint.hpp>
#include <catalyst_stub.h>

#include <adios2.h>
#include <iostream>

enum adios_catalyst_status
{
  adios_catalyst_status_invalid_node = 100,
  adios_catalyst_status_results = 101,
};
#define catalyst_err(name) static_cast<enum catalyst_status>(adios_catalyst_status_##name)

static AdiosPipeline* Internal;

//-----------------------------------------------------------------------------
enum catalyst_status catalyst_initialize_adios(const conduit_node* params)
{
  const conduit_cpp::Node cpp_params = conduit_cpp::cpp_node(const_cast<conduit_node*>(params));

  // Initialize the writer configured with the adios xml config file
  if (cpp_params.has_path("adios"))
  {
    auto& adios_config_filepath = cpp_params["adios"];
    if (adios_config_filepath.number_of_children() != 1)
    {
      std::cerr << "Wrong number of adios config file, we should only have one file, the adios.xml "
                   "but we have "
                << adios_config_filepath.number_of_children() << " scripts." << std::endl;
      return catalyst_err(invalid_node);
    }

    auto& adios_xml = adios_config_filepath.child(0);
    std::string adios_filename;
    if (adios_xml.dtype().is_string())
    {
      adios_filename = adios_xml.as_string();
    }
    else
    {
      if (adios_xml.has_child("filename"))
      {
        adios_filename = adios_xml["filename"].as_string();
      }
    }

    if (adios_filename.find(".xml") == std::string::npos)
    {
      std::cerr << "You need to specify also an xml file with at least the xml path inside"
                << std::endl;
      return catalyst_err(invalid_node);
    }

    std::string catalyst_conduit(catalyst_conduit_node_to_json(params));

    // Catalyst / Adios
    Internal = new AdiosPipeline();
    if (!Internal->Initialize(adios_filename, catalyst_conduit))
    {
      std::cerr << "Error when we initialize the in transit pipeline" << std::endl;
      return catalyst_err(invalid_node);
    }
  }

  return catalyst_status_ok;
}

//-----------------------------------------------------------------------------
enum catalyst_status catalyst_execute_adios(const conduit_node* params)
{
  const conduit_cpp::Node cpp_params = conduit_cpp::cpp_node(const_cast<conduit_node*>(params));
  if (!cpp_params.has_path("catalyst"))
  {
    std::cerr << "Path catalyst isn't provided, skipping." << std::endl;
    return catalyst_err(invalid_node);
  }

  const auto& root = cpp_params["catalyst"];
  const int timestep = root.has_path("state/timestep")
    ? root["state/timestep"].to_int64()
    : (root.has_path("state/cycle") ? root["state/cycle"].to_int64() : 0);
  const double time = root.has_path("state/time") ? root["state/time"].to_float64() : 0;

  // Catalyst / Adios
  if (Internal && !Internal->Execute(timestep, root))
  {
    std::cerr << "Error when we execute the in transit pipeline" << std::endl;
    return catalyst_err(invalid_node);
  }

  return catalyst_status_ok;
}

//-----------------------------------------------------------------------------
enum catalyst_status catalyst_finalize_adios(const conduit_node* params)
{
  std::string catalyst_conduit(catalyst_conduit_node_to_json(params));

  // Catalyst / Adios
  if (Internal && Internal->Finalize(catalyst_conduit))
  {
    delete Internal;
    Internal = nullptr;

    return catalyst_status_ok;
  }

  return catalyst_err(invalid_node);
}

//-----------------------------------------------------------------------------
enum catalyst_status catalyst_about_adios(conduit_node* params)
{
  catalyst_status status = catalyst_stub_about(params);
  conduit_node_set_path_char8_str(params, "catalyst/implementation", "adios");

  return status;
}

//-----------------------------------------------------------------------------
enum catalyst_status catalyst_results_adios(conduit_node* params)
{
  return catalyst_stub_results(params);
}
