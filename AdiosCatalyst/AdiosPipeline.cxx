#include "AdiosPipeline.h"

#include <catalyst.h>
#include <catalyst_api.h>

#include <algorithm>

#include <mpi.h>

namespace
{
//----------------------------------------------------------------------------
// Used to debug conduit_node, print only the conduit hierarchy name without any details.
void PrintHierarchy(const conduit_cpp::Node root, const std::string& spacing = "")
{
  auto nbChild = root.number_of_children();
  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    const auto& child = root.child(i);
    std::cout << spacing << child.name() << std::endl;
    PrintHierarchy(child, spacing + " ");
  }
}

//----------------------------------------------------------------------------
// Convenient conduit method to recursively extract a specific node by name from a parent node.
bool FindNode(conduit_cpp::Node node, conduit_cpp::Node& output, const std::string nodeName)
{
  auto nbChild = node.number_of_children();
  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    conduit_cpp::Node child = node.child(i);
    if (child.name() == nodeName)
    {
      output = child;
      return true;
    }

    if (::FindNode(child, output, nodeName))
    {
      return true;
    }
  }

  return false;
}

//----------------------------------------------------------------------------
// Convenient method to retrieve number of point for cell based on their definitions
int GetNumberOfPointsForCellType(const std::string& shape)
{
  if (shape == "point")
  {
    return 1;
  }
  else if (shape == "line")
  {
    return 2;
  }
  else if (shape == "tri")
  {
    return 3;
  }
  else if (shape == "quad" || shape == "tetra")
  {
    return 4;
  }
  else if (shape == "hex")
  {
    return 8;
  }

  return 0;
}

//----------------------------------------------------------------------------
// Convenient method to define variable
template <typename T>
void DefineVariable(std::unordered_map<std::string, adios2::Variable<T>>& container, adios2::IO io,
  std::string name, const adios2::Dims& shape, const adios2::Dims& start, const adios2::Dims& count)
{
  if (container.find(name) != container.end())
  {
    return;
  }

  adios2::Variable<T> var;
  var = io.DefineVariable<T>(name, shape, start, count);
  container.insert(std::pair<std::string, adios2::Variable<T>>(name, var));
}

//----------------------------------------------------------------------------
// Convenient method to define variable
template <typename T>
void DefineVariable(
  std::unordered_map<std::string, adios2::Variable<T>>& container, adios2::IO io, std::string name)
{
  if (container.find(name) != container.end())
  {
    return;
  }

  container.insert(std::pair<std::string, adios2::Variable<T>>(name, io.DefineVariable<T>(name)));
}
}

//----------------------------------------------------------------------------
AdiosPipeline::AdiosPipeline()
  : MetaData(new AdiosData())
  , StateData(new AdiosData())
  , CoordsetsData(new AdiosData())
  , TopologicalData(new AdiosData())
  , FieldsData(new AdiosData())
{
}

//----------------------------------------------------------------------------
bool AdiosPipeline::Initialize(
  const std::string& adiosFileName, const std::string& catalystInitializeParametersAsString)
{
  if (adiosFileName.empty())
  {
    MPI_Abort(MPI_COMM_WORLD, -1);
    std::cerr << "File path specify to locate adios xml is missing." << std::endl;
    return false;
  }

  this->AdiosFileName = adiosFileName;
  this->CatalystInitializeParametersAsString = catalystInitializeParametersAsString;
  this->FirstExecute = true;

  // Do nothing more here because we didn't have a conduit_node with variables descriptions
  return true;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::Execute(int timestep, conduit_cpp::Node root)
{
  // Because we didn't have the root node during the catalyst initialize method, we initialize all
  // variables before we treat the first execute call.
  if (this->FirstExecute)
  {
    if (!this->InitializeVariables(root))
    {
      std::cerr << "Can't initialize all variables correctly." << std::endl;
      return false;
    }

    this->FirstExecute = false;
  }

  return this->Put(root, timestep);
}

//----------------------------------------------------------------------------
bool AdiosPipeline::Finalize(const std::string& catalystFinalizeParametersAsString)
{
  if (!catalystFinalizeParametersAsString.empty())
  {
    this->IO.DefineAttribute<std::string>(
      "CatalystFinalizeParameters", catalystFinalizeParametersAsString);
  }

  this->Writer.Close();

  this->Count.clear();
  this->Shape.clear();
  this->Start.clear();

  return true;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::InitializeVariables(conduit_cpp::Node channel)
{
  try
  {
    int rank, procs, wrank;
    MPI_Comm comm, cart_comm;

    // Initialize MPI
    MPI_Comm_rank(MPI_COMM_WORLD, &wrank);

    const unsigned int color = 1;
    MPI_Comm_split(MPI_COMM_WORLD, color, wrank, &comm);

    MPI_Comm_rank(comm, &rank);
    MPI_Comm_size(comm, &procs);


    std::vector<int> dims = { this->GetNumberOfPoints(channel) };
    if (dims.empty())
    {
      std::cerr << "We doesn't find node 'dims' inside the node " << channel.name()
                << "We need it to be able to define each adios variables." << std::endl;
      return false;
    }
    int mpiDims[3] = {};

    std::vector<int> periods(dims.size(), 1);
    std::vector<int> coords(dims.size(), 1);

    // Dimension of process grid
    std::vector<size_t> np;
    // Coordinate of this rank in process grid
    std::vector<size_t> p;
    // Dimension of local array
    std::vector<size_t> size;
    // Offset of local array in the global array
    std::vector<size_t> offset;

    MPI_Dims_create(procs, dims.size(), mpiDims);
    for (int i = 0; i < dims.size(); i++)
    {
      np.emplace_back(mpiDims[i]);
    }

    MPI_Cart_create(comm, dims.size(), mpiDims, periods.data(), 0, &cart_comm);
    MPI_Cart_coords(cart_comm, rank, dims.size(), coords.data());

    for (int i = 0; i < dims.size(); i++)
    {
      p.emplace_back(coords[i]);
      np.emplace_back(mpiDims[i]);
      size.emplace_back(dims[i] / np[i]);
      if (p[i] < dims[i] % np[i])
      {
        size[i]++;
      }
      offset.emplace_back((dims[i] / np[i] * p[i]) + std::min(dims[i] % np[i], p[i]));
    }

    // Initialize Adios2
    this->Adios = adios2::ADIOS(this->AdiosFileName, comm);

    this->IO = this->Adios.DeclareIO("Writer");
    this->Writer = this->IO.Open("gs.bp", adios2::Mode::Write);

    // DefineVariable() supports only container of size_t
    this->Shape.resize(dims.size());
    this->Start.resize(dims.size());
    this->Count.resize(dims.size());
    std::transform(
      dims.begin(), dims.end(), this->Shape.begin(), [](int x) { return static_cast<size_t>(x); });
    std::transform(offset.begin(), offset.end(), this->Start.begin(),
      [](int x) { return static_cast<size_t>(x); });
    std::transform(
      size.begin(), size.end(), this->Count.begin(), [](int x) { return static_cast<size_t>(x); });

    // Find cell dimension
    this->NumberOfCell = this->GetNumberOfCell(channel);
    this->CellType = this->GetCellType(channel);
    this->CellsShape =
      this->GetCellsShape(channel) * ::GetNumberOfPointsForCellType(this->CellType);
    this->CellsStart =
      this->GetCellsStart(channel) * ::GetNumberOfPointsForCellType(this->CellType);
    this->CellsCount =
      this->GetCellsCount(channel) * ::GetNumberOfPointsForCellType(this->CellType);

    bool sucessfullyInitiliazed = this->FillStateData(channel, "catalyst");
    sucessfullyInitiliazed &= this->FillMetaData(channel, "catalyst");
    sucessfullyInitiliazed &= this->FillCoordsetsData(channel, "catalyst");
    sucessfullyInitiliazed &= this->FillTopologiesData(channel, "catalyst");

    conduit_cpp::Node fields;
    bool finded = ::FindNode(channel, fields, "fields");
    if (finded)
    {
      sucessfullyInitiliazed &= this->FillFieldsData(fields, "catalyst/channels/grid/data/fields");
    }

    if (sucessfullyInitiliazed)
    {
      // Set all attributes
      this->IO.DefineAttribute<std::string>(
        "CatalystInitializeParameters", this->CatalystInitializeParametersAsString);
    }
    return sucessfullyInitiliazed;
  }
  catch (const MPI::Exception& mpiError)
  {
    std::cerr << mpiError.Get_error_string() << std::endl;
    MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    return false;
  }
  catch (const std::exception& error)
  {
    std::cerr << error.what() << std::endl;
    return false;
  }
}

//----------------------------------------------------------------------------
bool AdiosPipeline::FillStateData(const conduit_cpp::Node& root, const std::string name)
{
  bool isInitialized = true;
  conduit_index_t nbChild = root.number_of_children();

  bool isStateData = name.find("state") != std::string::npos;
  if (nbChild == 0 && isStateData && this->SupportedType(root))
  {
    return this->FillVariables(this->StateData, root, name);
  }

  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    auto child = root.child(i);
    isInitialized &= this->FillStateData(child, name + "/" + child.name());
  }

  return isInitialized;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::FillMetaData(const conduit_cpp::Node& root, const std::string name)
{
  bool isInitialized = true;
  conduit_index_t nbChild = root.number_of_children();

  bool isMetaData =
    name.find("type") != std::string::npos && name.find("data") == std::string::npos;
  if (nbChild == 0 && isMetaData && this->SupportedType(root))
  {
    return this->FillVariables(this->MetaData, root, name);
  }

  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    auto child = root.child(i);
    isInitialized &= this->FillMetaData(child, name + "/" + child.name());
  }

  return isInitialized;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::FillCoordsetsData(const conduit_cpp::Node& root, const std::string name)
{
  bool isInitialized = true;
  conduit_index_t nbChild = root.number_of_children();

  bool isCoordsets = name.find("coordsets") != std::string::npos;
  if (nbChild == 0 && isCoordsets && this->SupportedType(root))
  {
    // Need explicitly the shape data
    if (name.find("values") != std::string::npos)
    {
      return this->FillVariables(
        this->CoordsetsData, root, name, this->Shape, this->Start, this->Count);
    }
    else
    {
      return this->FillVariables(this->CoordsetsData, root, name);
    }
  }

  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    auto child = root.child(i);
    isInitialized &= this->FillCoordsetsData(child, name + "/" + child.name());
  }

  return isInitialized;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::FillTopologiesData(const conduit_cpp::Node& root, const std::string name)
{
  bool isInitialized = true;
  conduit_index_t nbChild = root.number_of_children();

  bool isTopologies = name.find("topologies") != std::string::npos;
  if (nbChild == 0 && isTopologies && this->SupportedType(root))
  {
    // Need explicitly the shape data
    if (name.find("connectivity") != std::string::npos)
    {
      adios2::Dims shape;
      shape.push_back(this->CellsShape);
      adios2::Dims start;
      start.push_back(this->CellsStart);
      adios2::Dims count;
      count.push_back(this->CellsCount);

      return this->FillVariables(this->TopologicalData, root, name, shape, start, count);
    }
    else
    {
      return this->FillVariables(this->TopologicalData, root, name);
    }
  }

  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    auto child = root.child(i);
    isInitialized &= this->FillTopologiesData(child, name + "/" + child.name());
  }

  return isInitialized;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::FillFieldsData(const conduit_cpp::Node& root, const std::string name)
{
  bool isInitialized = true;
  conduit_index_t nbChild = root.number_of_children();

  for (conduit_index_t fieldId = 0; fieldId < nbChild; fieldId++)
  {
    auto field = root.child(fieldId);

    if (!field.has_child("association") || !field.has_child("topology"))
    {
      break;
    }

    std::string association = field["association"].as_string();
    std::string topology = field["topology"].as_string();

    for (conduit_index_t fieldProperty = 0; fieldProperty < field.number_of_children();
         fieldProperty++)
    {
      auto property = field.child(fieldProperty);
      std::string fullName = name + "/" + field.name() + "/" + property.name();

      if (property.name().find("values") != std::string::npos)
      {
        // Fields can be linked to points or cells
        if (association == "vertex")
        {
          if (property.number_of_children() != 0)
          {
            for (conduit_index_t i = 0; i < property.number_of_children(); i++)
            {
              auto component = property.child(i);
              isInitialized &= this->FillVariables(this->FieldsData, component,
                fullName + "/" + component.name(), this->Shape, this->Start, this->Count);
            }
          }
          else
          {
            isInitialized &= this->FillVariables(this->FieldsData, property, fullName,
              this->Shape, this->Start, this->Count);
          }
        }
        else
        {
          // Based on cell
          adios2::Dims shape;
          shape.push_back(this->CellsShape);
          adios2::Dims start;
          start.push_back(this->CellsStart);
          adios2::Dims offset;
          offset.push_back(this->CellsCount);

          isInitialized &= this->FillVariables(
            this->FieldsData, property, fullName, shape, start, offset);
        }
      }
      else
      {
        isInitialized &= this->FillVariables(this->FieldsData, property, fullName);
      }
    }
  }

  return isInitialized;
}

//----------------------------------------------------------------------------
int AdiosPipeline::GetNumberOfPoints(const conduit_cpp::Node& data)
{
  conduit_cpp::Node numPtsNode;
  if (::FindNode(data, numPtsNode, "number_of_points"))
  {
    return numPtsNode.as_uint64();
  }

  return 0;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::SupportedType(const conduit_cpp::Node& node)
{
  bool supported = false;
  supported |= node.dtype().name() == "char8_str";
  supported |= node.dtype().name() == "int16";
  supported |= node.dtype().name() == "int32";
  supported |= node.dtype().name() == "int64";
  supported |= node.dtype().name() == "uint16";
  supported |= node.dtype().name() == "uint32";
  supported |= node.dtype().name() == "uint64";
  supported |= node.dtype().name() == "float32";
  supported |= node.dtype().name() == "float64";
  supported |= node.dtype().name() == "std::string";

  return supported;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::FillVariables(
  std::shared_ptr<AdiosData> data, const conduit_cpp::Node& values, const std::string& name)
{
  auto conduitType = values.dtype().name();

  if (conduitType == "char8_str")
  {
    if (data->VarsChar.find(name) != data->VarsChar.end())
    {
      return true;
    }
    data->VarsChar.insert(std::pair<std::string, adios2::Variable<std::string>>(
      name, this->IO.DefineVariable<std::string>(name)));
  }
  else if (conduitType == "int16")
  {
    ::DefineVariable<short>(data->VarsShort, this->IO, name);
  }
  else if (conduitType == "int32")
  {
    ::DefineVariable<int>(data->VarsInt, this->IO, name);
  }
  else if (conduitType == "int64")
  {
    ::DefineVariable<long int>(data->VarsLongInt, this->IO, name);
  }
  else if (conduitType == "uint16")
  {
    ::DefineVariable<unsigned short>(data->VarsUShort, this->IO, name);
  }
  else if (conduitType == "uint32")
  {
    ::DefineVariable<unsigned int>(data->VarsUInt, this->IO, name);
  }
  else if (conduitType == "uint64")
  {
    ::DefineVariable<unsigned long int>(data->VarsULongInt, this->IO, name);
  }
  else if (conduitType == "float32")
  {
    ::DefineVariable<float>(data->VarsFloat, this->IO, name);
  }
  else if (conduitType == "float64")
  {
    ::DefineVariable<double>(data->VarsDouble, this->IO, name);
  }
  else
  {
    std::cerr << "Type of " << name << " is " << conduitType << " and it's not currently supported."
              << std::endl;
    return false;
  }

  return true;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::FillVariables(std::shared_ptr<AdiosData> data, const conduit_cpp::Node& values,
  const std::string& name, const adios2::Dims& shape, const adios2::Dims& start,
  const adios2::Dims& offset)
{
  auto conduitType = values.dtype().name();

  if (conduitType == "int16")
  {
    ::DefineVariable<short>(data->VarsShort, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "int32")
  {
    ::DefineVariable<int>(data->VarsInt, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "int64")
  {
    ::DefineVariable<long int>(data->VarsLongInt, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "uint16")
  {
    ::DefineVariable<unsigned short>(data->VarsUShort, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "uint32")
  {
    ::DefineVariable<unsigned int>(data->VarsUInt, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "uint64")
  {
    ::DefineVariable<unsigned long int>(data->VarsULongInt, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "float32")
  {
    ::DefineVariable<float>(data->VarsFloat, this->IO, name, shape, start, offset);
  }
  else if (conduitType == "float64")
  {
    ::DefineVariable<double>(data->VarsDouble, this->IO, name, shape, start, offset);
  }
  else
  {
    std::cerr << "values type " << conduitType << " isn't currently supported." << std::endl;
    return false;
  }

  return true;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::Put(conduit_cpp::Node root, int timestep)
{
  // We put only : metaData, coordsets and topologies for now
  conduit_cpp::Node stateNode, metaDataNode, coordsetsNode, topologiesNode, fieldsNode;
  ::FindNode(root, metaDataNode, "type");
  ::FindNode(root, stateNode, "state");
  ::FindNode(root, coordsetsNode, "coordsets");
  ::FindNode(root, topologiesNode, "topologies");
  ::FindNode(root, fieldsNode, "fields");

  this->Writer.BeginStep();

  bool sucessfullyPut =
    this->PreparePutVariables(this->StateData, stateNode, "catalyst/state");
  sucessfullyPut &= this->PreparePutVariables(
    this->MetaData, metaDataNode, "catalyst/channels/grid/type");
  sucessfullyPut &= this->PreparePutVariables(
    this->CoordsetsData, coordsetsNode, "catalyst/channels/grid/data/coordsets");
  sucessfullyPut &= this->PreparePutVariables(
    this->TopologicalData, topologiesNode, "catalyst/channels/grid/data/topologies");
  sucessfullyPut &= this->PreparePutVariables(
    this->FieldsData, fieldsNode, "catalyst/channels/grid/data/fields");

  this->Writer.EndStep();

  return sucessfullyPut;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::PreparePutVariables(
  std::shared_ptr<AdiosData> data, conduit_cpp::Node& root, std::string name)
{
  bool isInitialized = true;
  conduit_index_t nbChild = root.number_of_children();
  if (nbChild == 0 && this->SupportedType(root))
  {
    return this->PutVariables(data, root, name);
  }

  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    auto child = root.child(i);
    isInitialized &= this->PreparePutVariables(data, child, name + "/" + child.name());
  }

  return isInitialized;
}

//----------------------------------------------------------------------------
bool AdiosPipeline::PutVariables(
  std::shared_ptr<AdiosData> data, const conduit_cpp::Node& values, const std::string& name)
{
  try
  {
    auto conduitType = values.dtype().name();
    if (conduitType == "char8_str")
    {
      this->Writer.Put(
        data->VarsChar.find(name.c_str())->second, std::string(values.as_char8_str()));
    }
    else if (conduitType == "int16")
    {
      this->Writer.Put(data->VarsShort.find(name.c_str())->second, values.as_int16_ptr());
    }
    else if (conduitType == "int32")
    {
      this->Writer.Put(data->VarsInt.find(name.c_str())->second, values.as_int32_ptr());
    }
    else if (conduitType == "int64")
    {
      this->Writer.Put(data->VarsLongInt.find(name.c_str())->second, values.as_int64_ptr());
    }
    else if (conduitType == "uint16")
    {
      this->Writer.Put(data->VarsUShort.find(name.c_str())->second, values.as_uint16_ptr());
    }
    else if (conduitType == "uint32")
    {
      this->Writer.Put(data->VarsUInt.find(name.c_str())->second, values.as_uint32_ptr());
    }
    else if (conduitType == "uint64")
    {
      this->Writer.Put(data->VarsULongInt.find(name.c_str())->second, values.as_uint64_ptr());
    }
    else if (conduitType == "float32")
    {
      this->Writer.Put(data->VarsFloat.find(name.c_str())->second, values.as_float32_ptr());
    }
    else if (conduitType == "float64")
    {
      this->Writer.Put(data->VarsDouble.find(name.c_str())->second, values.as_float64_ptr());
    }
    else
    {
      std::cerr << "value type " << conduitType << " isn't currently supported." << std::endl;
      return false;
    }
  }
  catch (const MPI::Exception& mpiError)
  {
    std::cerr << mpiError.Get_error_string() << std::endl;
    MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    return false;
  }
  catch (const std::exception& error)
  {
    std::cerr << error.what() << std::endl;
    return false;
  }

  return true;
}

//----------------------------------------------------------------------------
int AdiosPipeline::GetNumberOfCell(const conduit_cpp::Node& data)
{
  conduit_cpp::Node numberOfCellNode;
  if (!::FindNode(data, numberOfCellNode, "number_of_cell"))
  {
    std::cerr << "Doesn't find node named 'number_of_cell'" << std::endl;
    return -1;
  }

  return numberOfCellNode.as_uint64();
}

//----------------------------------------------------------------------------
int AdiosPipeline::GetCellsShape(const conduit_cpp::Node& data)
{
  conduit_cpp::Node cellsShape;
  if (!::FindNode(data, cellsShape, "cells_shape"))
  {
    std::cerr << "Doesn't find node named 'cells_shape'" << std::endl;
    return 0;
  }

  return cellsShape.as_int32();
}

//----------------------------------------------------------------------------
int AdiosPipeline::GetCellsStart(const conduit_cpp::Node& data)
{
  conduit_cpp::Node cellsStart;
  if (!::FindNode(data, cellsStart, "cells_start"))
  {
    std::cerr << "Doesn't find node named 'cells_start'" << std::endl;
    return 0;
  }

  return cellsStart.as_int32();
}

//----------------------------------------------------------------------------
int AdiosPipeline::GetCellsCount(const conduit_cpp::Node& data)
{
  conduit_cpp::Node cellsCount;
  if (!::FindNode(data, cellsCount, "cells_count"))
  {
    std::cerr << "Doesn't find node named 'cells_count'" << std::endl;
    return 0;
  }

  return cellsCount.as_int32();
}

//----------------------------------------------------------------------------
std::string AdiosPipeline::GetCellType(const conduit_cpp::Node& data)
{
  conduit_cpp::Node numberOfCellNode;
  if (!::FindNode(data, numberOfCellNode, "shape"))
  {
    std::cerr << "Doesn't find node named 'shape'" << std::endl;
    return "";
  }

  return numberOfCellNode.as_string();
}
