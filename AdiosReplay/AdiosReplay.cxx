#include <catalyst.hpp>

#include <adios2.h>

#include <mpi.h>

#include <iostream>
#include <string>

#include <map>

/**
 * Container used to store sperately all sub node data for 'channels', 'fields', 'topologies'.
 */
struct ReplayData
{
  std::map<std::string, std::string> stringVariables;
  std::map<std::string, short*> shortVariables;
  std::map<std::string, int*> intVariables;
  std::map<std::string, long int*> longIntVariables;
  std::map<std::string, unsigned short*> ushortVariables;
  std::map<std::string, unsigned int*> uintVariables;
  std::map<std::string, unsigned long int*> uLongIntVariables;
  std::map<std::string, float*> floatVariables;
  std::map<std::string, double*> doubleVariables;
};

namespace
{
//----------------------------------------------------------------------------
// Used to debug conduit_node, print only the conduit hierarchy name without any details.
void PrintHierarchy(const conduit_cpp::Node root, const std::string& spacing = "")
{
  auto nbChild = root.number_of_children();
  for (conduit_index_t i = 0; i < nbChild; i++)
  {
    const auto& child = root.child(i);
    std::cout << spacing << child.name() << std::endl;
    PrintHierarchy(child, spacing + " ");
  }
}

//----------------------------------------------------------------------------
template <typename T>
void FillVariable(std::pair<std::string, adios2::Params> variable, adios2::Engine& reader,
  adios2::IO io, std::map<std::string, T*>& container)
{
  try
  {
    T* data = nullptr;
    adios2::Variable<T> var = io.InquireVariable<T>(variable.first);

    if (var.Shape().size() > 0)
    {
      data = new T[var.Shape()[0]];
    }
    else
    {
      data = new T();
      ;
    }

    reader.Get<T>(var, data);
    container.insert(std::pair<std::string, T*>(variable.first, data));
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
    MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
  }
}

//----------------------------------------------------------------------------
void FillContainer(ReplayData& data, adios2::Engine reader, adios2::IO io,
  std::pair<std::string, adios2::Params> variable)
{
  // std::cout << variable.first << std::endl;
  auto setting = variable.second;
  if (setting["Type"] == "string")
  {
    try
    {
      std::string var;
      reader.Get<std::string>(variable.first, var);
      if (!var.empty())
      {
        data.stringVariables.insert(std::pair<std::string, std::string>(variable.first, var));
      }
    }
    catch (const std::exception& e)
    {
      std::cerr << e.what() << std::endl;
      MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    }
  }
  else if (setting["Type"] == "int16_t")
  {
    FillVariable<short>(variable, reader, io, data.shortVariables);
  }
  else if (setting["Type"] == "int32_t")
  {
    FillVariable<int>(variable, reader, io, data.intVariables);
  }
  else if (setting["Type"] == "int64_t")
  {
    FillVariable<long int>(variable, reader, io, data.longIntVariables);
  }
  else if (setting["Type"] == "uint16_t")
  {
    FillVariable<unsigned short>(variable, reader, io, data.ushortVariables);
  }
  else if (setting["Type"] == "uint32_t")
  {
    FillVariable<unsigned int>(variable, reader, io, data.uintVariables);
  }
  else if (setting["Type"] == "uint64_t")
  {
    FillVariable<unsigned long int>(variable, reader, io, data.uLongIntVariables);
  }
  else if (setting["Type"] == "float")
  {
    FillVariable<float>(variable, reader, io, data.floatVariables);
  }
  else if (setting["Type"] == "double")
  {
    FillVariable<double>(variable, reader, io, data.doubleVariables);
  }
  else
  {
    std::cerr << "Unsupported type : " << setting["Type"] << " from " << variable.first
              << " -> Skip" << std::endl;
  }
}

//----------------------------------------------------------------------------
template <typename T>
void FillConduit(conduit_cpp::Node& node, const std::map<std::string, T*>& variables)
{
  for (const auto& variable : variables)
  {
    node[variable.first].set(variable.second);
  }
}

//----------------------------------------------------------------------------
void FillNode(conduit_cpp::Node& node, const ReplayData& container)
{
  FillConduit(node, container.shortVariables);
  FillConduit(node, container.intVariables);
  FillConduit(node, container.longIntVariables);
  FillConduit(node, container.ushortVariables);
  FillConduit(node, container.uintVariables);
  FillConduit(node, container.uLongIntVariables);
  FillConduit(node, container.floatVariables);
  FillConduit(node, container.doubleVariables);
  for (auto variable : container.stringVariables)
  {
    node[variable.first].set(variable.second);
  }
}

//----------------------------------------------------------------------------
int GetNumberOfPoints(const ReplayData& coordsetData)
{
  for (const auto& variable : coordsetData.uLongIntVariables)
  {
    if (variable.first.find("number_of_points") != std::string::npos)
    {
      return variable.second[0];
    }
  }

  return 0;
}

//----------------------------------------------------------------------------
int GetNumberOfCell(const ReplayData& topologiesData)
{
  for (const auto& variable : topologiesData.uLongIntVariables)
  {
    if (variable.first.find("number_of_cell") != std::string::npos)
    {
      return variable.second[0];
    }
  }

  return 0;
}

//----------------------------------------------------------------------------
std::string GetFieldAssociation(const ReplayData& fieldsData, const std::string& fieldName)
{
  std::string fieldAssociation = "None";
  for (const auto& variable : fieldsData.stringVariables)
  {
    if (variable.first.find(fieldName) != std::string::npos &&
      variable.first.find("association") != std::string::npos)
    {
      fieldAssociation = variable.second;
      break;
    }
  }

  return fieldAssociation;
}

//----------------------------------------------------------------------------
template <typename T>
void FillValuesInZeroCopy(
  conduit_cpp::Node& root, const std::map<std::string, T*> variables, int number_of_element = 0)
{
  for (const auto& variable : variables)
  {
    if (variable.first.find("values") != std::string::npos ||
      variable.first.find("connectivity") != std::string::npos)
    {
      root[variable.first].set_external(variable.second, number_of_element);
    }
    else if (variable.first.find("number_of_cell") != std::string::npos ||
      variable.first.find("coords/dims") != std::string::npos)
    {
      // Do nothing
    }
    else
    {
      root[variable.first].set(variable.second);
    }
  }
}

//----------------------------------------------------------------------------
int GetNumberOfPointsForCellType(const std::string& shape)
{
  if (shape == "point")
  {
    return 1;
  }
  else if (shape == "line")
  {
    return 2;
  }
  else if (shape == "tri")
  {
    return 3;
  }
  else if (shape == "quad" || shape == "tetra")
  {
    return 4;
  }
  else if (shape == "hex")
  {
    return 8;
  }

  std::cerr << "WARNING | Unsupported shape : " << shape << std::endl;
  return 0;
}

//----------------------------------------------------------------------------
void FillCoordsetsInNode(
  conduit_cpp::Node& root, const ReplayData& coordsetsData, int number_of_points)
{
  // String information don't need to be offset
  for (const auto& variable : coordsetsData.stringVariables)
  {
    root[variable.first] = variable.second;
  }

  FillValuesInZeroCopy(root, coordsetsData.ushortVariables, number_of_points);
  FillValuesInZeroCopy(root, coordsetsData.uintVariables, number_of_points);
  FillValuesInZeroCopy(root, coordsetsData.uLongIntVariables, number_of_points);
  FillValuesInZeroCopy(root, coordsetsData.shortVariables, number_of_points);
  FillValuesInZeroCopy(root, coordsetsData.intVariables, number_of_points);
  FillValuesInZeroCopy(root, coordsetsData.longIntVariables, number_of_points);
  FillValuesInZeroCopy(root, coordsetsData.floatVariables, number_of_points);
  FillValuesInZeroCopy(root, coordsetsData.doubleVariables, number_of_points);
}

//----------------------------------------------------------------------------
void FillTopologiesInNode(conduit_cpp::Node& root, const ReplayData& topologicalData)
{
  int numCells = GetNumberOfCell(topologicalData);
  if (numCells == 0)
  {
    std::cerr << "Can't generate the topologie, there is 0 cell or the conduit node "
                 "'number_of_cell' is missing."
              << std::endl;
    return;
  }

  int numberOfPointsForCellType = 0;
  // String information don't need to have offset
  for (const auto& variable : topologicalData.stringVariables)
  {
    if (variable.first.find("shape") != std::string::npos)
    {
      numberOfPointsForCellType = GetNumberOfPointsForCellType(variable.second);
      if (numberOfPointsForCellType == 0)
      {
        std::cerr << "WARNING | Can't recreate the topological node." << std::endl;
        return;
      }
    }
    root[variable.first].set(variable.second);
  }

  int numCellPts = numCells * numberOfPointsForCellType;

  FillValuesInZeroCopy(root, topologicalData.ushortVariables, numCellPts);
  FillValuesInZeroCopy(root, topologicalData.uintVariables, numCellPts);
  FillValuesInZeroCopy(root, topologicalData.uLongIntVariables, numCellPts);
  FillValuesInZeroCopy(root, topologicalData.shortVariables, numCellPts);
  FillValuesInZeroCopy(root, topologicalData.intVariables, numCellPts);
  FillValuesInZeroCopy(root, topologicalData.longIntVariables, numCellPts);
  FillValuesInZeroCopy(root, topologicalData.floatVariables, numCellPts);
  FillValuesInZeroCopy(root, topologicalData.doubleVariables, numCellPts);
}

//----------------------------------------------------------------------------
void FillFieldsInNode(conduit_cpp::Node& root, const ReplayData& fieldsData,
  const std::string& fieldName, const ReplayData& topologicalData, int number_of_points)
{
  std::string fieldAssociation = GetFieldAssociation(fieldsData, fieldName);
  int number_of_element = 0;

  if (fieldAssociation == "element")
  {
    number_of_element = GetNumberOfCell(topologicalData);
  }
  else if (fieldAssociation == "vertex")
  {
    number_of_element = number_of_points;
  }

  if (number_of_element == 0)
  {
    return;
  }

  for (auto variable : fieldsData.stringVariables)
  {
    root[variable.first] = variable.second;
  }

  FillValuesInZeroCopy(root, fieldsData.ushortVariables, number_of_element);
  FillValuesInZeroCopy(root, fieldsData.uintVariables, number_of_element);
  FillValuesInZeroCopy(root, fieldsData.uLongIntVariables, number_of_element);
  FillValuesInZeroCopy(root, fieldsData.shortVariables, number_of_element);
  FillValuesInZeroCopy(root, fieldsData.intVariables, number_of_element);
  FillValuesInZeroCopy(root, fieldsData.longIntVariables, number_of_element);
  FillValuesInZeroCopy(root, fieldsData.floatVariables, number_of_element);
  FillValuesInZeroCopy(root, fieldsData.doubleVariables, number_of_element);
}
}

//----------------------------------------------------------------------------
// We just want to extract the conduit node stored inside the attribute and execute on it the
// catalyst implementation
catalyst_status replay_adios_initialize(adios2::IO io)
{
  std::string catalystNodeVariableName = "CatalystInitializeParameters";
  auto catalystAttribute = io.InquireAttribute<std::string>("CatalystInitializeParameters");

  if (catalystAttribute)
  {
    std::string catalystNodeJson = catalystAttribute.Data()[0];
    if (!catalystNodeJson.empty())
    {
      conduit_node* catalystNode = catalyst_conduit_node_create();
      catalyst_conduit_node_generate(catalystNode, catalystNodeJson.c_str(), "json", nullptr);

      if (catalystNode)
      {
        // We force to used the catalyst specific implementation for paraview for the reader
        catalyst_conduit_node_set_path_char8_str(
          catalystNode, "catalyst_load/implementation", "paraview");
        auto status = catalyst_initialize(catalystNode);
        return status;
      }
    }
  }

  std::cerr << "Catalyst Initialization failed" << std::endl;

  return catalyst_status::catalyst_status_error_incomplete;
}

//----------------------------------------------------------------------------
catalyst_status replay_adios_execute(adios2::IO io, adios2::Engine& reader)
{
  ReplayData metaData, statesData, coordsetsData, topologicalData;
  std::map<std::string, ReplayData> allFieldsData;

  auto variables = io.AvailableVariables();
  for (const auto& variable : variables)
  {
    adios2::Params setting = variable.second;

    if (setting.count("Type") == 0)
    {
      std::cerr << "All variable should have a 'Type' field but not " << variable.first
                << std::endl;
      continue;
    }

    if (variable.first.find("catalyst/channels/grid") != std::string::npos)
    {
      if (variable.first.find("coordsets") != std::string::npos)
      {
        ::FillContainer(coordsetsData, reader, io, variable);
      }
      else if (variable.first.find("topologies") != std::string::npos)
      {
        ::FillContainer(topologicalData, reader, io, variable);
      }
      else if (variable.first.find("fields") != std::string::npos)
      {
        // extract the field name
        int fieldPos = variable.first.find("fields");
        int slashPos = variable.first.find("/", fieldPos);
        std::string subFields = variable.first.substr(slashPos + 1);
        std::string fieldName = subFields.substr(0, subFields.find("/"));

        if (fieldName.empty())
        {
          continue;
        }

        if (allFieldsData.find(fieldName) == allFieldsData.end())
        {
          ReplayData newFieldData;
          ::FillContainer(newFieldData, reader, io, variable);

          allFieldsData.insert(std::pair<std::string, ReplayData>(fieldName, newFieldData));
        }
        else
        {
          ReplayData& fieldData = allFieldsData.find(fieldName)->second;
          ::FillContainer(fieldData, reader, io, variable);
        }
      }
      else
      {
        ::FillContainer(metaData, reader, io, variable);
      }
    }
    else if (variable.first.find("catalyst/state") != std::string::npos)
    {
      ::FillContainer(statesData, reader, io, variable);
    }
    else
    {
      // Mesh blueprint can also have particules node etc...
      std::cerr << "unsupported variable named '" << variable.first << "'." << std::endl;
    }
  }

  reader.EndStep();

  // All variables was filled, recreate the conduit node
  conduit_node* catalyst_node = catalyst_conduit_node_create();
  conduit_cpp::Node root = conduit_cpp::cpp_node(const_cast<conduit_node*>(catalyst_node));

  // Populate the node with all data that didn't need offset, stride...
  ::FillNode(root, statesData);
  ::FillNode(root, metaData);

  // Coordsets, topologies and fields need more specific definition
  // First we need to know each node dimension
  int number_of_points = ::GetNumberOfPoints(coordsetsData);
  ::FillCoordsetsInNode(root, coordsetsData, number_of_points);
  ::FillTopologiesInNode(root, topologicalData);

  for (const auto& pair : allFieldsData)
  {
    ::FillFieldsInNode(root, pair.second, pair.first, topologicalData, number_of_points);
  }

  return catalyst_execute(catalyst_node);
}

//----------------------------------------------------------------------------
catalyst_status replay_adios_finalize(adios2::IO io)
{
  conduit_node* catalystNode = catalyst_conduit_node_create();

  std::string catalystNodeVariableName = "CatalystFinalizeParameters";
  const auto& catalystAttribute =
    io.InquireAttribute<std::string>(catalystNodeVariableName.c_str());
  if (catalystAttribute)
  {
    std::string catalystNodeJson = catalystAttribute.Data()[0];
    if (!catalystNodeJson.empty())
    {
      catalyst_conduit_node_generate(catalystNode, catalystNodeJson.c_str(), "json", nullptr);
    }
  }

  return catalyst_finalize(catalystNode);
}

int main(int argc, char* argv[])
{

  MPI_Init(&argc, &argv);

  if (argc < 2)
  {
    std::cerr << "You need to specify an adios xml" << std::endl;
    return EXIT_FAILURE;
  }

  std::string xmlConfigFile = argv[1];
  if (xmlConfigFile.find(".xml") == std::string::npos)
  {
    std::cerr << "Specify the xml config file for adios as first parameter in the command line, "
                 "actually we got : "
              << xmlConfigFile << std::endl;
    return EXIT_FAILURE;
  }

  std::uint64_t communicator = static_cast<std::uint64_t>(MPI_Comm_c2f(MPI_COMM_WORLD));
  adios2::ADIOS adiosReader(argv[1], communicator);

  // Setup adios
  adios2::IO ioReader = adiosReader.DeclareIO("Reader");
  // XXX: this is the default sst file name generated by the writer part, user may want to specify
  // the name.
  adios2::Engine reader = ioReader.Open("gs.bp", adios2::Mode::Read);
  catalyst_status status = catalyst_status::catalyst_status_ok;

  // Default timeout value in SST
  bool isInitialized = false;
  for (int step = 0;; step++)
  {
    adios2::StepStatus readStatus = reader.BeginStep(adios2::StepMode::Read);
    if (readStatus != adios2::StepStatus::OK)
    {
      break;
    }

    if (!isInitialized)
    {
      status = replay_adios_initialize(ioReader);
      if (status != catalyst_status::catalyst_status_ok)
      {
        reader.Close();
        MPI_Finalize();
        return EXIT_FAILURE;
      }

      isInitialized = true;
    }

    status = replay_adios_execute(ioReader, reader);
    if (status != catalyst_status::catalyst_status_ok)
    {
      std::cerr << "catalyst_execute : step " << reader.CurrentStep() << " failed." << std::endl;

      reader.Close();
      MPI_Finalize();
      return EXIT_FAILURE;
    }
  }

  status = replay_adios_finalize(ioReader);
  if (status != catalyst_status::catalyst_status_ok)
  {
    std::cerr << "catalyst_finalize failed : " << status << std::endl;
  }
  reader.Close();

  MPI_Finalize();
  return 0;
}
